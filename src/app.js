import router from "../src/routes";
import config from "./config";
import mongoose from "mongoose";
import express from "express";
import cors from "cors";
const { Client, LocalAuth, List, Buttons } = require("whatsapp-web.js");
const qrcode = require("qrcode-terminal");

const client = new Client({
  puppeteer: {
    headless: false,
  },
  authStrategy: new LocalAuth({
    clientId: "YOUR_CLIENT_ID_1",
  }),
});
const app = express();

const port = process.env.PORT || 5000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.get("/",(req,res)=>{
  res.send("Working....!")
})

client.on("ready", () => {
  console.log("Client is ready!");
});

const productListOptions = {
  apple: "Apple",
  mango: "Mango",
  banana: "Banana",
};

let button = new Buttons(
  "Button body",
  [{ body: "Aceptar" }, { body: "rechazar" }],
  "title",
  "footer"
);

client.on("message", (message) => {
  console.log("==========>", message.body);
  if (message.body === "How are you?") {
    message.reply("I don't have feelings");
  }
});
client.on("message", (message) => {
  console.log("==========>", message.body);
  if (
    message.body === "Hii" ||
    message.body === "Hey" ||
    message.body === "Hiii"
  ) {
    message.reply("Hello!  I am Miren, Your nodejs guide");
    
    const options =
      "Reply with the number of the product you're interested in:\n" +
      "1. Apple\n" +
      "2. Mango\n" +
      "3. Banana";
    message.reply(options);
  }
});

client.on("message", (message) => {
  console.log("==========>", message.body);
  if (/^\d+$/.test(message.body)) {
    // Handle the user's choice based on the received number
    const choice = parseInt(message.body, 10);
    switch (choice) {
      case 1:
        message.reply("You selected Apple");
        break;
      case 2:
        message.reply("You selected Mango");
        break;
      case 3:
        message.reply("You selected Banana");
        break;
      default:
        message.reply("Invalid choice. Please reply with a valid number.");
    }
  }
});

client.initialize();

client.on("qr", (qrCode) => {
  console.log("Scan this QR code with your phone:");
  qrcode.generate(qrCode, { small: true });
  console.log(qrCode);
});

client.on("authenticated", (session) => {
  console.log("Authenticated");
});
app.use(router);
app.use(cors());

mongoose.connect(config.mongodbUrl(), {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(function (req, res, next) {
  const error = new Error("Not Found");
  error.status = 404;
  next(error);
});


if (app.get("env") === "development") {
  app.use(function (error, req, res, next) {
    res.status(error.status || 500);
    res.send({
      message: error.message,
      error: error,
    });
  });
}

// Production error handler
// No stacktraces leaked to user
app.use(function (error, req, res, next) {
  res.status(error.status || 500);
  res.send({
    message: error.message,
    error: error,
  });
});

app.listen(port, () =>
  console.log(`Server is running on  http://localhost:${port}`)
);

export default app;
